<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">

<head>

<base href="<?php print $GLOBALS['base_url']; ?>/" />

<title><?php print $head_title ?></title>

<?php print $head ?>

<?php print $styles ?>

<?php print $scripts ?>

<style type="text/css" media="print">@import "<?php print base_path() . path_to_theme() ?>/print.css";</style>

<!--[if lt IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/fix-ie.css";</style>
<![endif]-->

</head>

<body class="sidebar-right">

<div id="jump-content">
<a href="#center" title="<?php print t('skip directly to the content') ?>" accesskey="2"><?php print t('skip to content') ?></a>
</div>

<div id="wrapper">
<div id="container" class="clear-block">

<div id="header">

<div id="logo-floater">
<?php
  // Prepare header
  if ($site_name)
    $site_name_title = check_plain($site_name);
  if ($site_slogan)
    $site_slogan_title = check_plain($site_slogan);

  $site_title = $site_name_title ? $site_name_title : ($site_slogan_title ? $site_slogan_title : null);

  if ($logo)
    print '<a href="'. check_url($base_path) .'" title="'. $site_title .'" class="site-logo"><img src="'. check_url($logo) .'" alt="'. $site_title .'" /></a>';

  if ($site_title) {
    print '<a href="'. check_url($base_path) .'" title="'. $site_title .'" class="site-title"><h1>';
    if ($site_name_title)
      print '<span class="site-name">'. $site_name_title .'</span>'. ($site_slogan_title ? ' ' : '');
    if ($site_slogan_title)
      print '<span class="site-slogan">'. $site_slogan_title .'</span>';
    print '</h1></a>';
  }
?>

</div>

<?php
  if (!isset($primary_links) || empty($primary_links)) {
    $primary_links = array();
    $primary_links['menu-none'] = array('title' => '');
  }
  print theme('links', $primary_links, array('class' => 'links primary-links'));
?>

<div id="login"> 
     <?php if (isset($secondary_links)) : ?>
     <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
     <?php endif; ?>
</div>
<?php if ($search_box): ?>
<div id="search-floater" class="noprint">
<?php print $search_box ?>
</div>
<?php endif; ?>

</div>

<div id="center"><div id="squeeze"><div class="inner">

<?php if ($breadcrumb): print $breadcrumb; endif; ?>
<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>

<div id="header-region">
<?php print $header; ?>

</div>

<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>

<?php if ($title): print '<h2 class="main-title'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h2>'; endif; ?>

<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul>'; endif; ?>

<?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>


<div class="inner-content">
<?php if ($help): print $help; endif; ?>
<?php if ($messages): print $messages; endif; ?>
<?php print $content ?>
</div>
<?php if ($tabs): print '</div>'; endif; ?>

<span class="clear"></span>

<?php print $feed_icons ?>

</div></div></div>

<div id="language_bar">
              <?php
        // We don't need to check if we have more than one language because with removing
        // the current, we end up with an empty list if only one language, and thus, show nothing
        if(FALSE){
        $current = i18n_get_lang();
        
        $languages = language_list('enabled');

        $links = array();
        foreach ($languages[1] as $language) {
        	if ($language->language != $current) {
        		$links[$language->language] = array(
        		'href'       => $_GET['q'],
        		'title'      => $language->native,
        		'language'   => $language,
        		'attributes' => array('class' => 'language-link'),
        		);

        		// use the i18n function to set language name and/or flag according to the i18n settings:
        		//languageicons_link_add($links[$language->language]);

        		// Or do your own stuff, e.g. set the flags and no lang names,
        		// no matter what the i18n icon settings say.
        		if ($icon = theme('languageicons_icon', $language, NULL)) {
        			$links[$language->language]['title'] = theme('languageicons_place', $link['title'], $icon);
        			$links[$language->language]['html'] = TRUE;
        		}
        	}
        }
			}
        // Instead of calling languageicons_link_add above, we should be able to call
        // drupal_alter('translation_link', $links, $_GET['q']);
        // to add the flags or remove the lang names, according to the icon settings.
        // Didn't work for me, I got *all* languages shown again and even two flags for the
        // current one. Somehow interfers with the automatic adding of the language
        // flags in nodes bodies, because it works on a language neutral page

        // now format as you like. E.g.
?>
           
            <?php 
        	//	echo theme('links', $links, array('id'=>'imglinks' , 'class' => 'links secondary-links'));			
			?>
           
      </div>

<div id="sidebar-right">

      



<?php if ($right): print $right; endif; ?>
<?php if ($left): print $left; endif; ?>
</div>



</div>
<div id="footer">
<div id="footer_container">
<div id="left">
        <p class="contact_us">Contact us</p>
        <p class="address">
            DuoFertility,
            <br/>
            Cambridge Temperature Concepts Ltd,
            <br/>
            23 Cambridge Science Park,
            <br/>
            Milton Road,
            <br/>
            Cambridge,
            <br/>
            CB40EY,<br/>
            United Kingdom.
         </p>
</div><div id="right">
	<img alt="DuoFertility Logo" src="<?php echo check_url($base_path); ?>sites/all/themes/DuoFertility/images/footer.gif"/>
</div>
<div id="center_foot">
	<?php print $footer_message ?>
</div>
</div>
</div>

</div>

<?php print $closure ?>

</body>

</html>

